import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://perqara.com/')

WebUI.waitForElementVisible(findTestObject('Login/Page_Perqara - Konsultasi Hukum Gratis/p_Pengumuman'), 0)

WebUI.click(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/i_Kebijakan Privasi_fal fa-times text-2xl'))

WebUI.click(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/button_Masuk'))

WebUI.verifyElementVisible(findTestObject('Login/Page_Perqara - Konsultasi Hukum Gratis/p_Masuk'))

WebUI.setText(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/input_Email_input border-2 focusborder-secondary'), 
    'xx@xx')

WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/input_Sandi_input border-2 focusborder-secondary'), 
    'a1YqozJa/IlJvKoWMLQGHw==')

WebUI.click(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/button_Masuk_1'))

WebUI.verifyElementVisible(findTestObject('Login/Page_Perqara - Konsultasi Hukum Gratis/div_Oops.Email atau Sandi Tidak SesuaiOKCancel'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Login/Page_Perqara - Konsultasi Hukum Gratis/label kata sandi tidak sesuai'), 'Email atau Sandi Tidak Sesuai')

WebUI.click(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/div_Oops.Email atau Sandi Tidak SesuaiOKCancel'))

WebUI.click(findTestObject('Object Repository/Login/Page_Perqara - Konsultasi Hukum Gratis/button_OK'))

WebUI.verifyElementNotVisible(findTestObject('Login/Page_Perqara - Konsultasi Hukum Gratis/div_Oops.Email atau Sandi Tidak SesuaiOKCancel'))

