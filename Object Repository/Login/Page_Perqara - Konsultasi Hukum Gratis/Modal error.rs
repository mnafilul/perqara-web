<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Modal error</name>
   <tag></tag>
   <elementGuidId>8e42df5a-a92b-4793-90d1-7cc2eabca4ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal2-popup.swal2-modal.swal2-icon-error.swal2-show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4e243a69-57b1-466a-ab4c-5ad9064efffc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>swal2-title</value>
      <webElementGuid>b9d9242b-4725-4ddf-82e3-ccedd230f10a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>swal2-content</value>
      <webElementGuid>eeb7d77e-671f-4519-a87d-b7b762ce3405</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal2-popup swal2-modal swal2-icon-error swal2-show</value>
      <webElementGuid>0e9b3ddf-7249-4af1-8177-e1dd2119f065</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>a4d6ce2d-7ec6-4a9d-ba1b-edd7ebb6c4e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>d93603b2-b0f1-44aa-8a03-0bdce270b8b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-live</name>
      <type>Main</type>
      <value>assertive</value>
      <webElementGuid>df724108-d65c-4647-bfb5-b6cfd2d056f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-modal</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bd1e58fa-eaaa-465f-8824-dd369af11854</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel</value>
      <webElementGuid>38f2266a-3815-4b5e-b02d-713e7e1b05d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;swal2-shown swal2-height-auto&quot;]/body[@class=&quot;swal2-shown swal2-height-auto&quot;]/div[@class=&quot;swal2-container swal2-center swal2-backdrop-show&quot;]/div[@class=&quot;swal2-popup swal2-modal swal2-icon-error swal2-show&quot;]</value>
      <webElementGuid>ae79d773-5ac2-457e-b12c-e28cfb5a68ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[3]</value>
      <webElementGuid>9ff4f3cd-ddee-4b2d-81cb-5800823439ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[3]</value>
      <webElementGuid>c5c34e7a-a33d-4ee9-9b51-b4cde230b940</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>b6c6c879-6fa6-40f6-81fa-2433d1834713</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel' or . = '
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel')]</value>
      <webElementGuid>43a6dfa0-eee9-40db-bf04-510c711fd667</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
