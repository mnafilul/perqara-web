<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Oops.Email atau Sandi Tidak SesuaiOKCancel</name>
   <tag></tag>
   <elementGuidId>ae7e4f0f-7bc4-4f9a-9989-b51fee500c2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal2-popup.swal2-modal.swal2-icon-error.swal2-show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kebijakan Privasi'])[5]/following::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b42f89be-5fef-42ef-af2b-8d6ea3fcdfef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>swal2-title</value>
      <webElementGuid>99763f7c-5859-4008-bdd6-61af615e8a3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>swal2-content</value>
      <webElementGuid>2181605d-a941-435d-8842-0578a4fdf3c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal2-popup swal2-modal swal2-icon-error swal2-show</value>
      <webElementGuid>ec1aff68-7232-4a44-a0e9-aaa6173e40bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>85c343dc-ed6e-4307-a329-41ed35fd26d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>4c6afd48-c438-4fa9-aebb-64e7229740d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-live</name>
      <type>Main</type>
      <value>assertive</value>
      <webElementGuid>c94a8cb6-89bb-4789-8d03-70be1711f3ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-modal</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4cef405b-d0c8-4fa2-b722-20e7615d78d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel</value>
      <webElementGuid>535b93d6-36e5-4397-9874-b7377eec3d8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;swal2-shown swal2-height-auto&quot;]/body[@class=&quot;swal2-shown swal2-height-auto&quot;]/div[@class=&quot;swal2-container swal2-center swal2-backdrop-show&quot;]/div[@class=&quot;swal2-popup swal2-modal swal2-icon-error swal2-show&quot;]</value>
      <webElementGuid>8ec44d5b-d2d3-43cd-9610-929b2130c073</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kebijakan Privasi'])[5]/following::div[7]</value>
      <webElementGuid>dc295d13-0e2d-46a8-b6ac-a54f5640aa4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='FAQ'])[5]/following::div[9]</value>
      <webElementGuid>0413d6b1-f7b4-4a63-9ef0-d10315a67a2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>85084eb4-cce9-4956-80e4-03ac8b7d67e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel' or . = '
        
        
      
    Oops...!×Email atau Sandi Tidak SesuaiOKCancel')]</value>
      <webElementGuid>11474475-f7c9-40b5-837f-a330bc4de72c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
